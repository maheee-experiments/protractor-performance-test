exports.config = {
  framework: 'jasmine',
  directConnect: true,
  specs: [
    './src/**/*.spec.js'
  ],
  capabilities: {
    'browserName': 'chrome',
    'loggingPrefs': {
        'performance': 'ALL'
    }
  }
}
