const pm = require('./perfmodel.js');

describe('workspace-project App', () => {

  beforeEach(async () => {
    await browser.restart();
  });

  it('should display welcome message', async () => {
    browser.driver.get('http://www.mahes.net');
  });

  afterEach(async () => {
    const perfLogs = await browser.manage().logs().get('performance');
    const perfData = pm.parsePerformanceLog(perfLogs);
    pm.renderPerformanceData(perfData);
  });

});
