
class PerfModelRequest {
  constructor(frameId, requestId) {
    this.frameId = frameId;
    this.requestId = requestId;

    this.started = null;
    this.ended = null;

    this.url = null;
    this.cached = null;
  }
}

class PerfModelFrame {
  constructor(frameId) {
    this.frameId = frameId;

    this.started = null;
    this.ended = null;

    this.requestEvents = [];
    this.navigationEvents = [];
  }
}

class PerfModel {
  constructor() {
    this.frameMap = new Map();
    this.requestMap = new Map();
    this.frameEvents = [];
  }

  _addFrame(frameId) {
    const frame = new PerfModelFrame(frameId);

    this.frameMap.set(frameId, frame);
    this.frameEvents.push(frame);

    return frame;
  }

  _addRequest(frameId, requestId) {
    const request = new PerfModelRequest(frameId, requestId);

    this.requestMap.set(requestId, request);
    this.frameMap.get(frameId).requestEvents.push(request);

    return request;
  }

  getFrame(frameId) {
    const frame = this.frameMap.get(frameId);
    return frame ? frame : this._addFrame(frameId);
  }

  getRequest(frameId, requestId) {
    this.getFrame(frameId);
    const request = this.requestMap.get(requestId);
    return request ? request : this._addRequest(frameId, requestId);
  }

}

function parsePerformanceLog(perfLogs) {
  const perfModel = new PerfModel();

  for (const line of perfLogs) {
    const message = JSON.parse(line.message).message;
    let request, frame;

    switch (message.method) {
      case 'Network.requestWillBeSent':
        request = perfModel.getRequest(message.params.frameId, message.params.requestId);
        request.started = line.timestamp; // message.params.timestamp
        request.url = message.params.request.url;
        break;

      case 'Network.loadingFinished':
        request = perfModel.getRequest(message.params.frameId, message.params.requestId);
        request.ended = line.timestamp; // message.params.timestamp
        request.cached = false;
        break;

      case 'Network.requestServedFromCache':
        request = perfModel.getRequest(message.params.frameId, message.params.requestId);
        request.ended = line.timestamp; // message.params.timestamp
        request.cached = true;
        break;

      case 'Page.frameStartedLoading':
        frame = perfModel.getFrame(message.params.frameId);
        frame.started = line.timestamp;
        break;

      case 'Page.frameStoppedLoading':
        frame = perfModel.getFrame(message.params.frameId);
        frame.ended = line.timestamp;
        break;

      case 'Page.frameNavigated':
        frame = perfModel.getFrame(message.params.frameId);
        frame.navigationEvents.push({
          started: line.timestamp,
          url: message.params.frame.url
        });
        break;

      default:
        break;
    }
  }

  return perfModel;
}

function renderPerformanceData(perfModel) {
  console.log();
  console.log();

  for (const perfFrame of perfModel.frameEvents) {
    if (perfFrame.requestEvents.length === 0) {
      continue;
    }

    console.log('Frame loaded in', (perfFrame.ended - perfFrame.started) + 'ms'); // perfFrame.navigationEvents

    for (const perfRequest of perfFrame.requestEvents) {
      if (perfRequest.cached) {
        console.log('  ', 'cached', perfRequest.url);
      } else if (!perfRequest.started || !perfRequest.ended) {
        console.log('  ', '???', perfRequest.url);
      } else {
        console.log('  ', (perfRequest.ended - perfRequest.started) + 'ms', perfRequest.url);
      }
    }

    console.log();
  }
}

exports.PerfModel = PerfModel;
exports.parsePerformanceLog = parsePerformanceLog;
exports.renderPerformanceData = renderPerformanceData;
